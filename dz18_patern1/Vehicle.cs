﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz18_patern1
{
    /// <summary>
    /// Транспортное средство
    /// speed - скорость
    /// weight - вес 
    /// width - ширина
    /// length - длинна
    /// </summary>
    public class Vehicle : ICloneable, IMyCloneable
    {
        public int speed;
        public int weight;
        public int width;
        public int length;

        public Vehicle(int speed, int weight, int width, int length)
        {
            this.speed = speed;
            this.weight = weight;
            this.width = width;
            this.length = length;
        }

        public virtual object Clone()
        {
            return MyClone();
        }

        public virtual object MyClone()
        {
            return new Vehicle(speed, weight, width, length);
        }

        public override string ToString()
        {
            return $"speed={speed}; weight={weight}; width={width}; length={length}";
        }
    }


}
