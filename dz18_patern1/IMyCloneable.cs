﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz18_patern1
{
    public interface IMyCloneable
    {
        object MyClone();
    }
}
