﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz18_patern1
{
    /// <summary>
    /// Автомобиль
    /// number - номер
    /// color - цвет 
    /// </summary>
    public class Car : Vehicle
    {
        public string number;
        public string color;
        public Car(int speed, int weight, int width, int length, string number, string color) : base(speed, weight, width, length)
        {
            this.number = number;
            this.color = color;                 
        }

        public override object MyClone()
        {
            Vehicle vehicle = (Vehicle)base.MyClone();
            return new Car(vehicle.speed, vehicle.weight,vehicle.width, vehicle.length, this.number, this.color);
        }

        public override string ToString()
        {
            return base.ToString()+ $" number={number}; color={color};";
        }
    }
}
