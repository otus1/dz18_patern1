﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz18_patern1
{
    /// <summary>
    /// Грузовик
    /// carryingCapacity - это грузоподъемность
    /// </summary>
    public class Truck: Car
    {
        public int carryingCapacity { get; set; }

        public Truck(int speed, int weight, int width, int length, string number, string color, int carryingCapacity): base (speed, weight, width, length,  number, color)
        {
            this.carryingCapacity = carryingCapacity;
        }

        public override object MyClone()
        {
            Car car = (Car)base.MyClone();
            return new Truck(car.speed, car.weight, car.weight, car.length, car.number, car.color, carryingCapacity);
        }


        public override string ToString()
        {
            return base.ToString() + $" CarryingCapacity={carryingCapacity};";
        }
    }
}
