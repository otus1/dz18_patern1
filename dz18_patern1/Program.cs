﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dz18_patern1
{
    class Program
    {
        static void Main(string[] args)
        {
            //===========================================================================
            var vehicle = new Vehicle(100, 1200, 2, 4);
            var vehicle1 = (Vehicle) vehicle.Clone();
            vehicle1.speed = 120;
            Console.WriteLine(vehicle);
            Console.WriteLine(vehicle1);

            var car = new Car(200, 1200, 2, 4,"x000Cx","green");
            var car1 = (Car)car.MyClone();
            car1.number = "o321rf";
            Console.WriteLine(car);
            Console.WriteLine(car1);

            var track = new Truck(200, 1200, 2, 4, "x000Cx", "green", 20);
            var track1 = (Truck)track.MyClone();
            track1.carryingCapacity= 22;
            Console.WriteLine(track);
            Console.WriteLine(track1);

            //===========================================================================
            Console.ReadKey();

        }
    }
}

